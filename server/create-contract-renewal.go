package server

import (
	"log"
	"strconv"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func CreateContractRenewal(ctx *fasthttp.RequestCtx) {

	contractid, _ := strconv.Atoi(ctx.UserValue("contractid").(string))

	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}
	a := grpcdal.LeasityServicesMockDAL{}

	renewalReport, err := a.GetContractRenewalReportById(contractid, tenantid)
	if err != nil {
		return
	}
	renewalReport.TenantId = tenantid

	err = LeasityDB.CreateNewReportIntoDb(renewalReport, "CONTRACT_RENEWAL")
	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println(renewalReport)

}
