package server

import (
	"encoding/json"
	"log"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func GetDocumentById(ctx *fasthttp.RequestCtx) {
	a := grpcdal.LeasityServicesMockDAL{}
	renewalReport, err := a.GetContractRenewalReportById(1, "wdgwg")
	if err != nil {
		return
	}
	log.Println(renewalReport)
}

func GetAdminDocumentsByTenantId(ctx *fasthttp.RequestCtx) {
	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}
	reports, err := LeasityDB.GetAdminReportsByTenantId(tenantid)
	if err != nil {
		log.Println(err.Error())
		return
	}
	jsonOutput, err := json.Marshal(reports)
	if err != nil {
		log.Println(err.Error())
		ctx.Error(err.Error(), 500)
		return
	}
	ctx.Write(jsonOutput)
}
