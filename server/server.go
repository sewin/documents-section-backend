package server

import (
	"log"
	"os"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"leasity.cl/backends/documents-section/internal/dw"
)

/*const (
	host     = "leasity-production-db.cyzb8xgnijnj.sa-east-1.rds.amazonaws.com"
	dbport   = 9623
	user     = "leasity_sebastian"
	password = "jE8?a5JP#AbqV7wr"
	dbname   = "leasity"
)*/

var LeasityDB *dw.DataBaseManager
var PaymentsDB *dw.DataBaseManager

type FastHTTPServer struct {
	ListenAddr string
	Router     *router.Router
}

func NewFastHttpServer(addr string) *FastHTTPServer {
	r := router.New()

	return &FastHTTPServer{ListenAddr: addr, Router: r}
}

func (s *FastHTTPServer) Run() {
	var err error
	LeasityDB, err = dw.NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		log.Fatalln("Error connecting to leasity DB " + " : " + err.Error())
		return
	}

	PaymentsDB, err = dw.NewDataBaseManager(os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_USER_PASSWORD"), os.Getenv("PAYMENTS_DB_NAME"), os.Getenv("DB_PORT"))
	if err != nil {

		log.Fatalln("Error connecting to payments DB " + " : " + err.Error())
		return
	}

	if err := LeasityDB.Connect(); err != nil {
		log.Fatalf("Could not connect to")
		return
	}
	defer LeasityDB.Close()

	if err := PaymentsDB.Connect(); err != nil {
		log.Fatalf("Could not connect to")
		return
	}
	defer PaymentsDB.Close()

	s.Router.POST("/api/documents/contract-renewal/{contractId}", CreateContractRenewal)
	s.Router.POST("/api/documents/contract-readjustment/{contractId}", CreateContractReadjustment)
	s.Router.POST("/api/documents/owner-liquidation-report/{ownerId}", CreateOwnerRentLiquidatonReport)
	s.Router.POST("/api/documents/warranty-liquidation", CreateWarrantyLiquidationReport)
	s.Router.POST("/api/documents/voucher-payment-receipt/{voucherId}", CreateVoucherPaymentReceipt)

	s.Router.DELETE("/api/documents/{documentId}", DeleteDocumentById)
	s.Router.PUT("/api/documents/{documentId}/unfile", UnfileDocumentById)

	s.Router.GET("/api/documents/", GetAdminDocumentsByTenantId)

	log.Println("server listening")
	if err := fasthttp.ListenAndServe(s.ListenAddr, s.Router.Handler); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}

}
