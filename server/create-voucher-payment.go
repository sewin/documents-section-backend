package server

import (
	"log"
	"strconv"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func CreateVoucherPaymentReceipt(ctx *fasthttp.RequestCtx) {

	voucherid, _ := strconv.Atoi(ctx.UserValue("voucherid").(string))

	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}
	a := grpcdal.LeasityServicesMockDAL{}

	voucherPaymentReport, err := a.GetVoucherPaymentReceipt(voucherid, tenantid)
	if err != nil {
		return
	}
	voucherPaymentReport.TenantId = tenantid

	err = LeasityDB.CreateNewReportIntoDb(voucherPaymentReport, "VOUCHER_PAYMENT")
	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println(voucherPaymentReport)

}
