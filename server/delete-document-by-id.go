package server

import (
	"log"
	"strconv"

	"github.com/valyala/fasthttp"
)

func DeleteDocumentById(ctx *fasthttp.RequestCtx) {
	documentId, _ := strconv.Atoi(ctx.UserValue("documentid").(string))

	deletedId, err := LeasityDB.ChangeReportDeletedStatusById(documentId, true)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Document Id: ", deletedId, " was succesfully deleted")

}

func UnfileDocumentById(ctx *fasthttp.RequestCtx) {
	documentId, _ := strconv.Atoi(ctx.UserValue("documentid").(string))

	deletedId, err := LeasityDB.ChangeReportDeletedStatusById(documentId, false)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Document Id: ", deletedId, " was succesfully unfiled")

}
