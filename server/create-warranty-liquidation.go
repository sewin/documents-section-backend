package server

import (
	"log"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func CreateWarrantyLiquidationReport(ctx *fasthttp.RequestCtx) {

	var err error
	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}

	a := grpcdal.LeasityServicesMockDAL{}

	warrantyLiquidationReport, err := a.GetWarrantyLiquidationReport(tenantid)
	if err != nil {
		return
	}
	warrantyLiquidationReport.TenantId = tenantid

	err = LeasityDB.CreateNewReportIntoDb(warrantyLiquidationReport, "WARRANTY_LIQUIDATION")
	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println(warrantyLiquidationReport)

}
