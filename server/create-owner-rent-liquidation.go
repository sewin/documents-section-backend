package server

import (
	"log"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func CreateOwnerRentLiquidatonReport(ctx *fasthttp.RequestCtx) {

	contractid, _ := strconv.Atoi(ctx.UserValue("ownerid").(string))
	var err error
	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}

	UntilDate := string(ctx.QueryArgs().Peek("untilDate"))
	if UntilDate != "" {
		_, err = time.Parse("2006-01-02", UntilDate)
	} else {
		UntilDate = time.Now().Format("2006-01-02")

	}
	if err != nil {
		ctx.Error("Error parsing UntilDate, wrong format", 400)
		log.Println("Error parsing UntilDate: " + err.Error())
		return
	}
	SinceDate := string(ctx.QueryArgs().Peek("sinceDate"))
	if SinceDate != "" {
		_, err = time.Parse("2006-01-02", SinceDate)
	} else {
		SinceDate = time.Now().AddDate(0, -1, 0).Format("2006-01-02")

	}
	if err != nil {
		ctx.Error("Error parsing SinceDate, wrong format", 400)
		log.Println("Error parsing SinceDate: " + err.Error())
		return
	}

	a := grpcdal.LeasityServicesMockDAL{}

	ownerLiquidationReport, err := a.GetOwnerRentLiquidationReport(contractid, tenantid, SinceDate, UntilDate)
	if err != nil {
		log.Println(err.Error())
		return
	}
	ownerLiquidationReport.TenantId = tenantid

	err = LeasityDB.CreateNewReportIntoDb(ownerLiquidationReport, "OWNER_LIQUIDATION")
	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println(ownerLiquidationReport)

}
