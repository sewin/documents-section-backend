package server

import (
	"log"
	"strconv"

	"github.com/valyala/fasthttp"
	grpcdal "leasity.cl/backends/documents-section/gRPC-DAL"
)

func CreateContractReadjustment(ctx *fasthttp.RequestCtx) {

	contractid, _ := strconv.Atoi(ctx.UserValue("contractid").(string))

	tenantid := string(ctx.Request.Header.Peek("X-Tenant-Id"))
	if tenantid == "" {
		ctx.Error("Not authenticated user", 404)
		log.Println("No tenant-id present in request header")
		return
	}
	a := grpcdal.LeasityServicesMockDAL{}

	readjustmentReport, err := a.GetContractReadjustmentReportById(contractid, tenantid)
	if err != nil {
		return
	}
	readjustmentReport.TenantId = tenantid

	err = LeasityDB.CreateNewReportIntoDb(readjustmentReport, "CONTRACT_READJUSTMENT")
	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println(readjustmentReport)

}
