package grpcdal

import "leasity.cl/backends/documents-section/internal/model"

func NewLeasityServicesGrpcDal() {

}

type LeasityDocumentServicesDAL interface {
	GetContractRenewalReportById(contractId int, tenantId string) (*model.ContractRenewalTemplateInfo, error)
	GetContractReadjustmentReportById(contractId int, tenantId string) (*model.ContractReadjustmentTemplateInfo, error)
}

type LeasityServicesMockDAL struct {
}
