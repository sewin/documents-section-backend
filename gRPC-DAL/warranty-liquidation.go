package grpcdal

import (
	"encoding/json"

	"leasity.cl/backends/documents-section/internal/model"
)

func (*LeasityServicesMockDAL) GetWarrantyLiquidationReport(tenantId string) (*model.WarrantyLiquidationTemplateInfo, error) {
	crt := model.WarrantyLiquidationTemplateInfo{}
	err := json.Unmarshal([]byte(warrantyLiquidationJson), &crt)
	return &crt, err
}

var warrantyLiquidationJson string = `{"propertyStreet":"","propertyStreetNumber":"","propertyApartment":"","propertyCommune":"","propertyType":"","ownerFirstName":"",
"ownerSurname":"","ownerSecondSurname":"","ownerTaxId":"","bankAccountHolderName":"","bankAccountTaxId":"","accountBank":"","bankAccountNumber":"","bankAccountType":"",
"accountEmail":"","adminFirstName":"","adminSurname":"","adminSecondSurname":"","adminTaxId":"","date":"24-10-2022","tenantId":"174e9313-02f1-4be3-b7b9-10e3d57b1cc8",
"companyName":"ValProp S.A","companyTaxEmail":"contacto@valprop.cl","lesseeFirstName":"","lesseeSurname":"","lesseeSecondSurname":"","contractEndsAt":"",
"ownerFullName":"Cristián Castro","lesseeFullName":"Cristiano Ronaldo","warrantyEntries":[{"amount":"$10.000","date":"02-05-2022",
"description":"Multa por estacionamiento"},{"amount":"$100.000","date":"05-04-2022","description":"Reparación de baldosas cocina"},
{"amount":"-$36.000","date":"03-06-2022","description":"Mantención eléctrica"}],"propertyAddress":"Juan de dios 2015, depto 14","contractEndDate":"01-01-2023",
"warrantyAmount":"$450.000","discountAmount":"$74.000","paymentAmount":"$376.000","contractId":"2277","noWarrantyEntries":false}`
