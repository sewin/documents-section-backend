package grpcdal

import (
	"encoding/json"

	"leasity.cl/backends/documents-section/internal/model"
)

func (*LeasityServicesMockDAL) GetVoucherPaymentReceipt(voucherId int, tenantId string) (*model.ContractRenewalTemplateInfo, error) {
	crt := model.ContractRenewalTemplateInfo{}
	err := json.Unmarshal([]byte(voucherPaymentJson), &crt)
	return &crt, err
}

var voucherPaymentJson string = `{"propertyStreet":"Quilin","propertyStreetNumber":"107","propertyApartment":"10E","propertyCommune":"MACUL",
"propertyType":"PARKING_LOT","ownerFirstName":"WH","ownerSurname":"INVERSIONES INMOBILIARIAS SPA","ownerSecondSurname":"",
"ownerTaxId":"77.060.933-K","lesseeFirstName":"ERIK ALEJANDRO","lesseeSurname":"TORRES","lesseeSecondSurname":"NUÑEZ",
"lesseeTaxId":"19.067.443-6","bankAccountHolderName":"Wenderhaus SpA","bankAccountTaxId":"77.368.574-6","accountBank":"SANTANDER",
"bankAccountNumber":"8306718-7","bankAccountType":"CC","accountEmail":"administraciones@whe.cl","adminFirstName":"Admin",
"adminSurname":"Wenderhaus","adminSecondSurname":"","adminTaxId":"77.368.574-6","paymentDay":"2021-07-08","voucherContractId":1849,
"date":"2022-10-04","tenantId":"c0c5a587-d96a-483a-86e5-4fd55e555771","bankAccountId":2025,"paymentAmount":"$55.000",
"voucherEntryDescriptions":[""],"voucherEntryAmounts":[55000],"voucherEntryTypes":["OWNER"],"voucherEntryCurrencies":["CLP"],
"ufValue":29733.57,"uffValue":29712.8,"uffDay":"2021-07-01","voucherId":0,"voucherPeriod":"7","entriesAmountsTable":[{"entryamount":"$55.000",
"entrydescription":"Arriendo","entryufval":"-","ufval":"-"}],"manuallyPaid":false,"readjustmentType":"RATE","companyName":"Wenderhaus SpA",
"companyTaxEmail":"administraciones@whe.cl","adminMail":"administraciones@whe.cl"}`
