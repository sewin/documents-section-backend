package grpcdal

import (
	"encoding/json"

	"leasity.cl/backends/documents-section/internal/model"
)

func (*LeasityServicesMockDAL) GetContractRenewalReportById(contractId int, tenantId string) (*model.ContractRenewalTemplateInfo, error) {
	crt := model.ContractRenewalTemplateInfo{}
	err := json.Unmarshal([]byte(renewaljson), &crt)
	return &crt, err
}

var renewaljson string = `{"propertyStreet":"San Antonio","propertyStreetNumber":"118-144","propertyApartment":"73","propertyCommune":"SANTIAGO",
"propertyType":"RETAIL","ownerFirstName":"Inversiones E Inmobiliaria Montalva Limitada","ownerSurname":".","ownerSecondSurname":"",
"ownerTaxId":"78.403.250-7","lesseeFirstName":"Moises Elias","lesseeSurname":"Chachati","lesseeSecondSurname":"","lesseeTaxId":"25.613.482-9",
"adminFirstName":"montalva","adminSurname":"quindos","adminSecondSurname":"","adminTaxId":"17.402.862-1","contractEndsAt":"2023-01-06",
"contractRenewalPeriod":12,"date":"2022-10-20","tenantId":"28437565-5b6e-4b79-b236-490032e206f7","startedAt":"2021-01-06","renewalEnabled":true,
"companyName":"Montalva Quindos","companyTaxEmail":"usuarioleasity@mq.cl","adminMail":"usuarioleasity@mq.cl"}`
