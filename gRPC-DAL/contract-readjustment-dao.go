package grpcdal

import (
	"encoding/json"

	"leasity.cl/backends/documents-section/internal/model"
)

func (*LeasityServicesMockDAL) GetContractReadjustmentReportById(contractId int, tenantId string) (*model.ContractReadjustmentTemplateInfo, error) {
	crt := model.ContractReadjustmentTemplateInfo{}
	err := json.Unmarshal([]byte(readjustmentjson), &crt)
	return &crt, err
}

var readjustmentjson string = `{"propertyStreet":"Vicuña Mackenna",
"propertyStreetNumber":"698","propertyApartment":"521","propertyCommune":"LA_CISTERNA","propertyType":"APARTMENT","ownerFirstName":"Inversiones El Tranque",
"ownerSurname":"SpA","ownerSecondname":"","ownerTaxId":"","lesseeFirstName":"Yean Carlos ","lesseeSurname":"Rivera ","lesseeSecondSurname":"Tovar","lesseeTaxId":"",
"adminFirstName":"Admin","adminSurname":"Wenderhaus","adminSecondSurname":"","adminTaxId":"77.368.574-6","readjustmentType":"CUFF","readjustmentRate":0,
"readjustmentPeriod":3,"readjustmentAmount":"$2.763","readjustedAmount":"$259.717","prevReadjustedAmount":"$256.954","readjustmentDate":"2021-07-01",
"contractReferenceDate":"2020-04-01","date":"2021-06-01","referenceAmount":250000,"tenantId":"c0c5a587-d96a-483a-86e5-4fd55e555771","totalReadjustmentRate":"",
"ipcReadjustmentTable":null,"ufReferenceAmount":"8,74","ufReferenceDate":"29.396,67","ufReadjustmentDate":"29.712,80","companyName":"Wenderhaus SpA",
"companyTaxEmail":"administraciones@whe.cl","adminMail":"administraciones@whe.cl","prevReadjustmentDate":"2021-04-01"}`
