package grpcdal

import (
	"encoding/json"

	"leasity.cl/backends/documents-section/internal/model"
)

func (*LeasityServicesMockDAL) GetOwnerRentLiquidationReport(contractId int, tenantId, sinceDate, untilDate string) (*model.OwnerRentLiquidationTemplateInfo, error) {
	crt := model.OwnerRentLiquidationTemplateInfo{}
	err := json.Unmarshal([]byte(ownerRentLiquidationJson), &crt)
	return &crt, err
}

var ownerRentLiquidationJson string = `{"propertyStreet":["Evaristo Lillo","Evaristo Lillo","Santa Beatriz","Evaristo Lillo","Avda Nueva Providencia",
"Avda Nueva Providencia","Chile España","Chile España","Evaristo Lillo","Francisco de Villagra","Avda Nueva Providencia","Evaristo Lillo"],
"propertyStreetNumber":["29","29","81","29","1372","1372","235","235","29","268","1372","29"],"propertyApartment":["175","135","605","145","604","904","810","910","165","903","113","155"],
"propertyCommune":["LAS_CONDES","LAS_CONDES","PROVIDENCIA","LAS_CONDES","PROVIDENCIA","PROVIDENCIA","NUNOA","NUNOA","LAS_CONDES","NUNOA","PROVIDENCIA",
"LAS_CONDES"],"propertyType":["APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT","APARTMENT",
"STORAGE_UNIT","APARTMENT"],"ownerFirstName":"Inversiones La Rioja","ownerSurname":".","ownerSecondSurname":"","ownerTaxId":"77.464.020-7",
"bankAccountHolderName":"Inversiones la Rioja","bankAccountTaxId":"77.464.020-7","accountBank":"SANTANDER","bankAccountNumber":"11132332",
"bankAccountType":"CC","accountEmail":"eduaheredia@yahoo.es","transactionId":"","adminFirstName":"Tattersall","adminSurname":"Gestión de Activos",
"adminSecondSurname":"","adminTaxId":"","ownerContractList":["7957","7703","7705","7723","7861","7959","7855","7862","7878","7950","8576","7731"],
"liquidationDate":["09-08-2022","12-08-2022","12-08-2022"],"voucherContractId":"","date":"2022-09-10","tenantId":"62011931-f0cf-448e-b5ab-bb9b397cbb33",
"bankAccountId":6524,"liquidationAmounts":[287870,438765,386296],"voucherIds":null,"voucherPeriods":["8","8","8"],"propertyTableNames":["Francisco de Villagra 268 depto 903",
"Evaristo Lillo 29 depto 145","Avda Nueva Providencia 1372 depto 904"],"sinceDate":"2022-08-06","untilDate":"2022-09-10","companyName":"Tattersall",
"companyTaxEmail":"arriendos.gda@tattersall.cl","adminMail":"arriendos.gda@tattersall.cl","liquidationIncomes":[450000,472500,416000],"liquidationExpenses":[162130,33735,29704]}`
