package dw

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"leasity.cl/backends/documents-section/internal/model"
)

var ErrNoEnvVar = errors.New("some of the needed env variables are not setted")

type DataBaseManager struct {
	connection       *pgxpool.Pool
	host             string
	dbname           string
	connectionstring string
}

func NewDataBaseManager(host, user, password, dbname, dbport string) (*DataBaseManager, error) {
	if host == "" || user == "" || password == "" || dbname == "" || dbport == "" {
		return nil, ErrNoEnvVar
	}
	return &DataBaseManager{
		host:             host,
		dbname:           dbname,
		connectionstring: fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s", host, dbport, user, password, dbname),
	}, nil
}

func (manager *DataBaseManager) Connect() error {
	log.Println("Connecting to DB at: ", manager.host)
	var err error
	manager.connection, err = pgxpool.Connect(context.Background(), manager.connectionstring)
	if err != nil {
		log.Println("DB connection failed:", err.Error())
		return err
	}
	log.Println("Connected to DB: ", manager.dbname)
	return nil
}

func (manager *DataBaseManager) Close() {
	log.Println("Closing DB connection ...")
	manager.connection.Close()
}

func (u *DataBaseManager) CreateNewReportIntoDb(rt model.ReportTemplate, template string) error {
	id := 0
	var err error
	time := time.Now()
	timestamp := time.String()[:22]
	log.Println(timestamp)

	body, _ := json.Marshal(rt)

	rows, err := u.connection.Query(context.Background(), `insert into documents(type, tenant_id, template_version,body,created_at) 
	values($1,$2, '1.0.0',$3,$4)
	returning id`, template, rt.GetTenantId(), body, timestamp)
	if err != nil {
		return err
	}
	for rows.Next() {

		err = rows.Scan(&id)

		if err != nil {
			return err
		}

	}
	log.Println(id)
	return err
}

func (u *DataBaseManager) ChangeReportDeletedStatusById(id int, deleted bool) (int, error) {
	row, err := u.connection.Query(context.Background(), `update documents set deleted = $1 where id = $2 returning id`, deleted, id)
	if err != nil {
		return 0, err
	}
	docId := 0
	for row.Next() {
		err = row.Scan(&docId)
		if err != nil {
			return 0, err
		}

	}

	return docId, nil
}

func (u *DataBaseManager) GetAdminReportsByTenantId(tenantId string) ([]DbReportModel, error) {
	rows, err := u.connection.Query(context.Background(), `select id, type, body,template_version,tenant_id,coalesce(deleted, false),
	TO_CHAR(created_at,'YYYY-MM-DD-HH24.MI.SS.FF6') from documents where tenant_id =$1`, tenantId)
	if err != nil {
		return nil, err
	}
	Reports := make([]DbReportModel, 0)
	for rows.Next() {
		report := DbReportModel{}
		err = rows.Scan(&report.Id, &report.Type, &report.Body, &report.TemplateVersion, &report.TenantId, &report.Deleted, &report.CreatedAt)
		if err != nil {
			return nil, err
		}
		Reports = append(Reports, report)
	}

	return Reports, nil
}

type DbReportModel struct {
	Id              int         `json:"id"`
	Type            string      `json:"type"`
	Body            interface{} `json:"body"`
	TemplateVersion string      `json:"template_version"`
	TenantId        string      `json:"tenant_id"`
	Deleted         bool        `json:"deleted"`
	CreatedAt       string      `json:"created_at"`
}
