package model

type AdminContractList struct {
	ContractIds              []string
	ContractReadjustmentDate []string
	ContractEndsAtDate       []string
	AdminId                  string
}

func NewAdminContractListFromList(ids []string) (*AdminContractList, error) {
	return &AdminContractList{ContractIds: ids}, nil
}
