package model

import (
	"sort"
	"strings"
	"time"

	"github.com/leekchan/accounting"
)

type OwnerRentLiquidationTemplateInfo struct {
	PropertyStreet        []string
	PropertyStreetNumber  []string
	PropertyApartment     []string
	PropertyCommune       []string
	PropertyType          []string
	OwnerFirstName        string
	OwnerSurname          string
	OwnerSecondSurname    string
	OwnerTaxId            string
	BankAccountHolderName string
	BankAccountTaxId      string
	AccountBank           string
	BankAccountNumber     string
	BankAccountType       string
	AccountEmail          string
	TransactionId         string
	AdminFirstName        string
	AdminSurname          string
	AdminSecondSurname    string
	AdminTaxId            string
	OwnerContractList     []string
	LiquidationDate       []string
	VoucherContractId     string
	Date                  string
	TenantId              string
	BankAccountId         int
	LiquidationAmounts    []float64
	VoucherIds            []int
	VoucherPeriods        []string
	PropertyTableNames    []string
	SinceDate             string
	UntilDate             string
	CompanyName           string
	CompanyTaxEmail       string
	AdminMail             string
	LiquidationIncomes    []float64
	LiquidationExpenses   []float64
}

func (orl *OwnerRentLiquidationTemplateInfo) LiquidationsSum() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	ls := 0.0
	for i := 0; i < len(orl.LiquidationAmounts); i++ {
		ls += orl.LiquidationAmounts[i]
	}
	return ac.FormatMoney(ls)
}

func (orl *OwnerRentLiquidationTemplateInfo) GetTenantId() string {
	return orl.TenantId
}

func (orl *OwnerRentLiquidationTemplateInfo) AdminFullName() string {

	return orl.AdminFirstName + " " + orl.AdminSurname + " " + orl.AdminSecondSurname
}

func (orl *OwnerRentLiquidationTemplateInfo) OwnerFullName() string {
	return orl.OwnerFirstName + " " + orl.OwnerSurname + " " + orl.OwnerSecondSurname
}

func (orl *OwnerRentLiquidationTemplateInfo) PropertyNames() []string {
	pnames := make([]string, 0)
	for i := 0; i < len(orl.PropertyStreet); i++ {
		pnames = append(pnames, orl.PropertyStreet[i]+" "+orl.PropertyStreetNumber[i]+" "+PropertyType[orl.PropertyType[i]]+" "+orl.PropertyApartment[i])

	}
	return pnames
}

func (orl *OwnerRentLiquidationTemplateInfo) PropertiesAmountsTable() []map[string]string {
	outtable := make([]map[string]string, len(orl.LiquidationAmounts))
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	ntrows := 0
	for i := 0; i < len(orl.LiquidationAmounts); i++ {

		if orl.LiquidationAmounts[i] != 0.0 {
			tablerow := make(map[string]string, 0)
			tablerow["propertyname"] = orl.PropertyTableNames[i]
			tablerow["propertyamount"] = ac.FormatMoney(orl.LiquidationAmounts[i])
			tablerow["liquidationincomes"] = ac.FormatMoney(orl.LiquidationIncomes[i])
			tablerow["liquidationexpenses"] = ac.FormatMoney(orl.LiquidationExpenses[i])
			tablerow["liquidationdate"] = orl.LiquidationDate[i]
			tablerow["voucherperiod"] = Months[orl.VoucherPeriods[i]]
			outtable[ntrows] = tablerow
			ntrows++
		}
	}
	return outtable[:ntrows]

}

func (orl *OwnerRentLiquidationTemplateInfo) Commune(i int) string {
	return GetCommune(orl.PropertyCommune[i])
}

func (orl *OwnerRentLiquidationTemplateInfo) FileName() string {

	return strings.ReplaceAll(orl.OwnerFullName(), ".", "") + ".pdf"
}

func (orl *OwnerRentLiquidationTemplateInfo) LogoUrl() string {
	return "https://files.leasity.cl/" + orl.TenantId + "/assets/summary-brand.png"
}

func (orl *OwnerRentLiquidationTemplateInfo) FormatDate() string {
	return Months[orl.Date[5:7]] + " de " + orl.Date[0:4]
}

func (orl *OwnerRentLiquidationTemplateInfo) FormatAccountBankName() string {
	return Banks[orl.AccountBank]
}

func (orl *OwnerRentLiquidationTemplateInfo) FormatBankAccountType() string {
	return BankAccountTypes[orl.BankAccountType]
}

func (orl *OwnerRentLiquidationTemplateInfo) LiquidationMonth() string {

	return Months[orl.Date[5:7]]
}

func (orl *OwnerRentLiquidationTemplateInfo) SortTable() error {
	table := make([]TableRow, len(orl.LiquidationAmounts))
	for i := 0; i < len(table); i++ {
		table[i].liquidationamount = &orl.LiquidationAmounts[i]
		table[i].liquidationdate = &orl.LiquidationDate[i]
		table[i].propertyname = &orl.PropertyTableNames[i]
		table[i].voucherperiod = &orl.VoucherPeriods[i]
		table[i].liquidationincome = &orl.LiquidationIncomes[i]
		table[i].liquidationexpense = &orl.LiquidationExpenses[i]
	}
	sort.Sort(PropertyTable(table))
	return nil
}

type TableRow struct {
	propertyname       *string
	liquidationamount  *float64
	liquidationdate    *string
	voucherperiod      *string
	liquidationincome  *float64
	liquidationexpense *float64
}

type PropertyTable []TableRow

func (t PropertyTable) Len() int { return len(t) }
func (t PropertyTable) Less(i, j int) bool {
	a, _ := time.Parse("02-01-2006", *t[i].liquidationdate)
	b, _ := time.Parse("02-01-2006", *t[j].liquidationdate)
	return a.Before(b)
}
func (t PropertyTable) Swap(i, j int) {
	*t[i].liquidationamount, *t[j].liquidationamount = *t[j].liquidationamount, *t[i].liquidationamount
	*t[i].propertyname, *t[j].propertyname = *t[j].propertyname, *t[i].propertyname
	*t[i].liquidationdate, *t[j].liquidationdate = *t[j].liquidationdate, *t[i].liquidationdate
	*t[i].voucherperiod, *t[j].voucherperiod = *t[j].voucherperiod, *t[i].voucherperiod
	*t[i].liquidationexpense, *t[j].liquidationexpense = *t[j].liquidationexpense, *t[i].liquidationexpense
	*t[i].liquidationincome, *t[j].liquidationincome = *t[j].liquidationincome, *t[i].liquidationincome
}

func (orl *OwnerRentLiquidationTemplateInfo) AssetsBaseUrl() string {
	return "http://" + baseURLAssets + "/internal/"
}

func (orl *OwnerRentLiquidationTemplateInfo) FormatSinceDate() string {
	a, _ := time.Parse("2006-01-02", orl.SinceDate)
	return a.Format("02-01-2006")
}

func (orl *OwnerRentLiquidationTemplateInfo) FormatUntilDate() string {
	a, _ := time.Parse("2006-01-02", orl.UntilDate)
	return a.Format("02-01-2006")
}

func (orl *OwnerRentLiquidationTemplateInfo) LeasityLogoUrl() string {
	return "https://cdn.leasity.cl/assets/images/logo/full-solid-small.png"
}
