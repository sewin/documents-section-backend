package model

import (
	"strings"
	"time"

	"github.com/leekchan/accounting"
)

type PaymentReceiptTemplateInfo struct {
	PropertyStreet           string              `json:"propertyStreet"`
	PropertyStreetNumber     string              `json:"propertyStreetNumber"`
	PropertyApartment        string              `json:"propertyApartment"`
	PropertyCommune          string              `json:"propertyCommune"`
	PropertyType             string              `json:"propertyType"`
	OwnerFirstName           string              `json:"ownerFirstName"`
	OwnerSurname             string              `json:"ownerSurname"`
	OwnerSecondSurname       string              `json:"ownerSecondSurname"`
	OwnerTaxId               string              `json:"ownerTaxId"`
	LesseeFirstName          string              `json:"lesseeFirstName"`
	LesseeSurname            string              `json:"lesseeSurname"`
	LesseeSecondSurname      string              `json:"lesseeSecondSurname"`
	LesseeTaxId              string              `json:"lesseeTaxId"`
	BankAccountHolderName    string              `json:"bankAccountHolderName,omitempty"`
	BankAccountTaxId         string              `json:"bankAccountTaxId,omitempty"`
	AccountBank              string              `json:"accountBank,omitempty"`
	BankAccountNumber        string              `json:"bankAccountNumber,omitempty"`
	BankAccountType          string              `json:"bankAccountType,omitempty"`
	AccountEmail             string              `json:"accountEmail,omitempty"`
	TransactionId            string              `json:"transactionId,omitempty"`
	AdminFirstName           string              `json:"adminFirstName"`
	AdminSurname             string              `json:"adminSurname"`
	AdminSecondSurname       string              `json:"adminSecondSurname"`
	AdminTaxId               string              `json:"adminTaxId"`
	PaymentDay               string              `json:"paymentDay"`
	VoucherContractId        int                 `json:"voucherContractId"`
	Date                     string              `json:"date"`
	TenantId                 string              `json:"tenantId"`
	BankAccountId            int                 `json:"bankAccountId"`
	PaymentAmount            string              `json:"paymentAmount"`
	VoucherEntryDescriptions []string            `json:"voucherEntryDescriptions"`
	VoucherEntryAmounts      []float64           `json:"voucherEntryAmounts"`
	VoucherEntryTypes        []string            `json:"voucherEntryTypes"`
	VoucherEntryCurrencies   []string            `json:"voucherEntryCurrencies"`
	UFValue                  float64             `json:"ufValue"`
	UFFValue                 float64             `json:"uffValue"`
	UFFDay                   string              `json:"uffDay"`
	VoucherId                int                 `json:"voucherId"`
	VoucherPeriod            string              `json:"voucherPeriod"`
	EntriesAmountsTable      []map[string]string `json:"entriesAmountsTable"`
	ManuallyPaid             bool                `json:"manuallyPaid"`
	ReadjustmentType         string              `json:"readjustmentType"`
	CompanyName              string              `json:"companyName"`
	CompanyTaxEmail          string              `json:"companyTaxEmail"`
	UFEntryExists            bool                `json:"ufEntryExists,omitempty"`
	AdminMail                string              `json:"adminMail"`
}

func (pr *PaymentReceiptTemplateInfo) AdminFullName() string {

	return pr.AdminFirstName + " " + pr.AdminSurname + " " + pr.AdminSecondSurname
}

func (pr *PaymentReceiptTemplateInfo) OwnerFullName() string {
	return pr.OwnerFirstName + " " + pr.OwnerSurname + " " + pr.OwnerSecondSurname
}

func (pr *PaymentReceiptTemplateInfo) LesseeFullName() string {
	return pr.LesseeFirstName + " " + pr.LesseeSurname + " " + pr.LesseeSecondSurname
}

func (pr *PaymentReceiptTemplateInfo) Commune() string {
	return GetCommune(pr.PropertyCommune)
}

func (pr *PaymentReceiptTemplateInfo) MonthVoucherPeriod() string {
	return Months[pr.VoucherPeriod]
}

func (pr *PaymentReceiptTemplateInfo) FileName() string {

	return strings.ReplaceAll(pr.LesseeFullName(), ".", "") + ".pdf" //CAMBIAR NOMBRE
}

func (pr *PaymentReceiptTemplateInfo) LogoUrl() string {
	return "https://files.leasity.cl/" + pr.TenantId + "/assets/summary-brand.png"
}

func (pr *PaymentReceiptTemplateInfo) FormatDate() string {
	return Months[pr.Date[5:7]] + " de " + pr.Date[0:4]
}

func (pr *PaymentReceiptTemplateInfo) FormatPaymentDate() string {
	a, _ := time.Parse("2006-01-02", pr.PaymentDay)
	return a.Format("02-01-2006")
}

func (pr *PaymentReceiptTemplateInfo) FormatAccountBankName() string {
	return Banks[pr.AccountBank]
}

func (pr *PaymentReceiptTemplateInfo) FormatBankAccountType() string {
	return BankAccountTypes[pr.BankAccountType]
}

func (pr *PaymentReceiptTemplateInfo) LiquidationMonth() string {

	return Months[pr.Date[5:7]]
}

func (pr *PaymentReceiptTemplateInfo) PropertyName() string {
	if pr.PropertyApartment == "" {
		return pr.PropertyStreet + " " + pr.PropertyStreetNumber + ", " + pr.Commune()
	}
	return pr.PropertyStreet + " " + pr.PropertyStreetNumber + " " + PropertyType[pr.PropertyType] + " " + pr.PropertyApartment + ", " + pr.Commune()

}

func (pr *PaymentReceiptTemplateInfo) GenerateEntriesAmountsTable() []map[string]string {
	outtable := make([]map[string]string, len(pr.VoucherEntryDescriptions))
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	ufstr := accounting.Accounting{Symbol: "", Precision: 2, Thousand: ".", Decimal: ","}
	ufvalformat := accounting.Accounting{Symbol: "$", Precision: 2, Thousand: ".", Decimal: ","}
	ntrows := 0
	rentrowamount := 0.0
	totalamount := 0.0
	rententriesuf := 0
	rentufval := 0.0
	feerowamount := 0.0
	feeufval := 0.0
	feeentriesnotuf := 0

	switch pr.ReadjustmentType {
	case "UFF":
		pr.UFEntryExists = true
		for i := 0; i < len(pr.VoucherEntryDescriptions); i++ {

			if pr.VoucherEntryTypes[i] == "OWNER" || pr.VoucherEntryTypes[i] == "AGENT" || pr.VoucherEntryTypes[i] == "EXTRA_RENTAL" || pr.VoucherEntryTypes[i] == "EXTRA_AGENT" {
				if pr.VoucherEntryCurrencies[i] == "UF" {
					rentrowamount += pr.VoucherEntryAmounts[i] * pr.UFFValue
					rentufval += pr.VoucherEntryAmounts[i]
					rententriesuf++
				} else {
					rentrowamount += pr.VoucherEntryAmounts[i]
					rentufval += pr.VoucherEntryAmounts[i] / pr.UFFValue

				}
			}
			if pr.VoucherEntryTypes[i] == "OWNER_LATE_FEE" || pr.VoucherEntryTypes[i] == "AGENT_LATE_FEE" {
				if pr.VoucherEntryCurrencies[i] == "UF" {
					feerowamount += pr.VoucherEntryAmounts[i] * pr.UFFValue
					feeufval += pr.VoucherEntryAmounts[i]
				} else {
					feerowamount += pr.VoucherEntryAmounts[i]
					feeentriesnotuf++
				}
			}
			if pr.VoucherEntryTypes[i] == "EXTRA" {
				tablerow := make(map[string]string, 0)
				tablerow["entrydescription"] = pr.VoucherEntryDescriptions[i]
				if pr.VoucherEntryCurrencies[i] == "UF" {
					tablerow["entryufval"] = ufstr.FormatMoney(pr.VoucherEntryAmounts[i])
					tablerow["ufval"] = ac.FormatMoney(pr.UFFValue)
					tablerow["entryamount"] = ac.FormatMoney(pr.VoucherEntryAmounts[i] * pr.UFFValue)
					totalamount += pr.VoucherEntryAmounts[i] * pr.UFFValue
				} else {
					tablerow["entryufval"] = "-"
					tablerow["ufval"] = "-"
					tablerow["entryamount"] = ac.FormatMoney(pr.VoucherEntryAmounts[i])
					totalamount += pr.VoucherEntryAmounts[i]
				}

				outtable[ntrows] = tablerow
				ntrows++
			}
		}

		if feerowamount != 0 {
			tablerow := make(map[string]string, 0)
			tablerow["entrydescription"] = "Multas"
			if feeentriesnotuf == 0 {
				tablerow["entryufval"] = ufstr.FormatMoney(feeufval)
				tablerow["ufval"] = ac.FormatMoney(pr.UFFValue)
			} else {
				tablerow["entryufval"] = "-"
				tablerow["ufval"] = "-"
			}
			tablerow["entryamount"] = ac.FormatMoney(feerowamount)
			totalamount += feerowamount
			outtable[ntrows] = tablerow
			ntrows++
		}
		renttablerow := make(map[string]string, 0)
		renttablerow["entrydescription"] = "Arriendo"
		renttablerow["entryufval"] = ufstr.FormatMoney(rentufval)
		renttablerow["ufval"] = ufvalformat.FormatMoney(pr.UFFValue)

		renttablerow["entryamount"] = ac.FormatMoney(rentrowamount)
		totalamount += rentrowamount
		outtable[ntrows] = renttablerow
		ntrows++

		pr.PaymentAmount = ac.FormatMoney(totalamount)
		pr.PaymentDay = pr.UFFDay
		return outtable[:ntrows]

	default:
		for i := 0; i < len(pr.VoucherEntryDescriptions); i++ {

			if pr.VoucherEntryTypes[i] == "OWNER" || pr.VoucherEntryTypes[i] == "AGENT" || pr.VoucherEntryTypes[i] == "EXTRA_RENTAL" || pr.VoucherEntryTypes[i] == "EXTRA_AGENT" {
				if pr.VoucherEntryCurrencies[i] == "UF" {
					rentrowamount += pr.VoucherEntryAmounts[i] * pr.UFValue
					rentufval += pr.VoucherEntryAmounts[i]
					rententriesuf++
					pr.UFEntryExists = true
				} else {
					rentrowamount += pr.VoucherEntryAmounts[i]
					rentufval += pr.VoucherEntryAmounts[i] / pr.UFValue

				}
			}
			if pr.VoucherEntryTypes[i] == "OWNER_LATE_FEE" || pr.VoucherEntryTypes[i] == "AGENT_LATE_FEE" {
				if pr.VoucherEntryCurrencies[i] == "UF" {
					feerowamount += pr.VoucherEntryAmounts[i] * pr.UFValue
					feeufval += pr.VoucherEntryAmounts[i]
					pr.UFEntryExists = true
				} else {
					feerowamount += pr.VoucherEntryAmounts[i]
					feeentriesnotuf++
				}
			}
			if pr.VoucherEntryTypes[i] == "EXTRA" {
				tablerow := make(map[string]string, 0)
				tablerow["entrydescription"] = pr.VoucherEntryDescriptions[i]
				if pr.VoucherEntryCurrencies[i] == "UF" {
					tablerow["entryufval"] = ufstr.FormatMoney(pr.VoucherEntryAmounts[i])
					tablerow["ufval"] = ac.FormatMoney(pr.UFValue)
					tablerow["entryamount"] = ac.FormatMoney(pr.VoucherEntryAmounts[i] * pr.UFValue)
					totalamount += pr.VoucherEntryAmounts[i] * pr.UFValue
					pr.UFEntryExists = true
				} else {
					tablerow["entryufval"] = "-"
					tablerow["ufval"] = "-"
					tablerow["entryamount"] = ac.FormatMoney(pr.VoucherEntryAmounts[i])
					totalamount += pr.VoucherEntryAmounts[i]
				}

				outtable[ntrows] = tablerow
				ntrows++
			}
		}
		if feerowamount != 0 {
			tablerow := make(map[string]string, 0)
			tablerow["entrydescription"] = "Multas"
			if feeentriesnotuf == 0 {
				tablerow["entryufval"] = ufstr.FormatMoney(feeufval)
				tablerow["ufval"] = ac.FormatMoney(pr.UFValue)
			} else {
				tablerow["entryufval"] = "-"
				tablerow["ufval"] = "-"
			}
			tablerow["entryamount"] = ac.FormatMoney(feerowamount)
			totalamount += feerowamount
			outtable[ntrows] = tablerow
			ntrows++
		}
		tablerow := make(map[string]string, 0)
		tablerow["entrydescription"] = "Arriendo"
		if rententriesuf != 0 {
			tablerow["entryufval"] = ufstr.FormatMoney(rentufval)
			tablerow["ufval"] = ufvalformat.FormatMoney(pr.UFValue)
		} else {
			tablerow["entryufval"] = "-"
			tablerow["ufval"] = "-"
		}
		tablerow["entryamount"] = ac.FormatMoney(rentrowamount)
		totalamount += rentrowamount
		outtable[ntrows] = tablerow
		ntrows++

		pr.PaymentAmount = ac.FormatMoney(totalamount)
		return outtable[:ntrows]

	}

}

func (pr *PaymentReceiptTemplateInfo) AssetsBaseUrl() string {
	return "http://" + baseURLAssets + "/internal/"
}

func (pr *PaymentReceiptTemplateInfo) SortEntriesAmountsTable() error {
	iteration := len(pr.EntriesAmountsTable) / 2
	for i := 0; i < iteration; i++ {
		buff := pr.EntriesAmountsTable[i]
		pr.EntriesAmountsTable[i] = pr.EntriesAmountsTable[len(pr.EntriesAmountsTable)-i-1]
		pr.EntriesAmountsTable[len(pr.EntriesAmountsTable)-i-1] = buff
	}
	return nil
}

func (pr *PaymentReceiptTemplateInfo) LeasityLogoUrl() string {
	return "https://cdn.leasity.cl/assets/images/logo/full-solid-small.png"
}
