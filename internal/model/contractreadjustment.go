package model

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/leekchan/accounting"
)

type ContractReadjustmentTemplateInfo struct {
	PropertyStreet        string
	PropertyStreetNumber  string
	PropertyApartment     string
	PropertyCommune       string
	PropertyType          string
	OwnerFirstName        string
	OwnerSurname          string
	OwnerSecondSurname    string
	OwnerTaxId            string
	LesseeFirstName       string
	LesseeSurname         string
	LesseeSecondSurname   string
	LesseeTaxId           string
	AdminFirstName        string
	AdminSurname          string
	AdminSecondSurname    string
	AdminTaxId            string
	ReadjustmentType      string
	ReadjustmentRate      int
	ReadjustmentPeriod    int
	ReadjustmentAmount    string
	ReadjustedAmount      string
	PrevReadjustedAmount  string
	ReadjustmentDate      string
	ContractReferenceDate string
	Date                  string
	ReferenceAmount       float64
	TenantId              string
	TotalReadjustmentRate string
	IPCReadjustmentTable  []map[string]interface{}
	UFReferenceAmount     string
	UFReferenceDate       string
	UFReadjustmentDate    string
	CompanyName           string
	CompanyTaxEmail       string
	AdminMail             string
	PrevReadjustmentDate  string
}

func (crt *ContractReadjustmentTemplateInfo) IsIPCORUF() bool {
	return crt.ReadjustmentType == "IPC" || crt.ReadjustmentType == "UF"
}

func (crt *ContractReadjustmentTemplateInfo) GetTenantId() string {
	return crt.TenantId
}

func (crt *ContractReadjustmentTemplateInfo) NextReadjustmentMonth() string {
	a, _ := time.Parse("2006-01-02", crt.ReadjustmentDate)
	a = a.AddDate(0, crt.ReadjustmentPeriod-1, 0)
	c := a.Format("2006-01-02")
	return Months[c[5:7]] + " de " + fmt.Sprint(a.Year())
}

func (crt *ContractReadjustmentTemplateInfo) FReferenceAmount() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	return ac.FormatMoney(crt.ReferenceAmount)
}

func (crt *ContractReadjustmentTemplateInfo) FReadjustmentAmount() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	intreferenceamount, _ := strconv.Atoi(crt.ReadjustmentAmount)
	return ac.FormatMoney(intreferenceamount)
}

func (crt *ContractReadjustmentTemplateInfo) FormatReadjustmentPeriod() string {
	return Period[crt.ReadjustmentPeriod]
}

func (crt *ContractReadjustmentTemplateInfo) IsIPC() bool {
	return crt.ReadjustmentType == "IPC"
}

func (crt *ContractReadjustmentTemplateInfo) ReadjustmentMonthYear() string {

	return Months[crt.ReadjustmentDate[5:7]] + " de " + crt.ReadjustmentDate[0:4]
}

func (crt *ContractReadjustmentTemplateInfo) ReadjustmentMonth() string {

	return Months[crt.ReadjustmentDate[5:7]]
}

func (crt *ContractReadjustmentTemplateInfo) AdminFullName() string {

	return crt.AdminFirstName + " " + crt.AdminSurname + " " + crt.AdminSecondSurname
}

func (crt *ContractReadjustmentTemplateInfo) OwnerFullName() string {
	return crt.OwnerFirstName + " " + crt.OwnerSurname + " " + crt.OwnerSecondSurname
}

func (crt *ContractReadjustmentTemplateInfo) LesseeFullName() string {
	return crt.LesseeFirstName + " " + crt.LesseeSurname + " " + crt.LesseeSecondSurname
}

func (crt *ContractReadjustmentTemplateInfo) PropertyName() string {
	if crt.PropertyApartment == "" {
		return crt.PropertyStreet + " " + crt.PropertyStreetNumber + ", " + crt.Commune()
	}
	return crt.PropertyStreet + " " + crt.PropertyStreetNumber + " " + PropertyType[crt.PropertyType] + " " + crt.PropertyApartment + ", " + crt.Commune()

}
func (crt *ContractReadjustmentTemplateInfo) Commune() string {
	return GetCommune(crt.PropertyCommune)
}

func (crt *ContractReadjustmentTemplateInfo) FileName() string {
	if crt.PropertyApartment == "" {
		return crt.PropertyStreet + "-" + crt.PropertyStreetNumber + ".pdf"
	}

	return strings.ReplaceAll(crt.PropertyStreet+"-"+crt.PropertyStreetNumber+"-"+PropertyType[crt.PropertyType]+crt.PropertyApartment, ".", "") + ".pdf"
}

func (crt *ContractReadjustmentTemplateInfo) LogoUrl() string {
	return "https://files.leasity.cl/" + crt.TenantId + "/assets/summary-brand.png"
}

func (crt *ContractReadjustmentTemplateInfo) FormatDate() string {
	return Months[crt.Date[5:7]] + " de " + crt.Date[0:4]
}

func (crt *ContractReadjustmentTemplateInfo) AssetsBaseUrl() string {
	return "http://" + baseURLAssets + "/internal/"
}

func (crt *ContractReadjustmentTemplateInfo) IPCReadjustment() bool {
	return crt.ReadjustmentType == "IPC"
}

func (crt *ContractReadjustmentTemplateInfo) UFReadjustment() bool {
	return crt.ReadjustmentType == "UFF" || crt.ReadjustmentType == "CUFF" || crt.ReadjustmentType == "UFC"
}

func (crt *ContractReadjustmentTemplateInfo) FixRateReadjustment() bool {
	return crt.ReadjustmentType == "RATE"
}

func (crt *ContractReadjustmentTemplateInfo) OneMonthReadjustment() bool {
	return crt.ReadjustmentPeriod == 1
}

func (crt *ContractReadjustmentTemplateInfo) FormatPrevReadjustmentDate() string {
	a, _ := time.Parse("2006-01-02", crt.PrevReadjustmentDate)

	return a.Format("02-01-2006")
}

func (crt *ContractReadjustmentTemplateInfo) FormatReadjustmentDate() string {
	a, _ := time.Parse("2006-01-02", crt.ReadjustmentDate)

	return a.Format("02-01-2006")
}

func (crt *ContractReadjustmentTemplateInfo) FormatContractReferenceDate() string {
	a, _ := time.Parse("2006-01-02", crt.ContractReferenceDate)

	return a.Format("02-01-2006")
}

func (crt *ContractReadjustmentTemplateInfo) LeasityLogoUrl() string {
	return "https://cdn.leasity.cl/assets/images/logo/full-solid-small.png"
}
