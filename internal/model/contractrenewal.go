package model

import (
	"strings"
)

type ContractRenewalTemplateInfo struct { //HACER CONSTRUCTOR Y CAMBIAR NOMBRE ContractRenewalInfo
	PropertyStreet        string
	PropertyStreetNumber  string
	PropertyApartment     string
	PropertyCommune       string
	PropertyType          string
	OwnerFirstName        string
	OwnerSurname          string
	OwnerSecondSurname    string
	OwnerTaxId            string
	LesseeFirstName       string
	LesseeSurname         string
	LesseeSecondSurname   string
	LesseeTaxId           string
	AdminFirstName        string
	AdminSurname          string
	AdminSecondSurname    string
	AdminTaxId            string
	ContractEndsAt        string
	ContractRenewalPeriod int
	Date                  string
	TenantId              string
	StartedAt             string
	RenewalEnabled        bool
	CompanyName           string
	CompanyTaxEmail       string
	AdminMail             string
}

func (crt *ContractRenewalTemplateInfo) GetTenantId() string {
	return crt.TenantId
}

func (crt *ContractRenewalTemplateInfo) AdminFullName() string {

	return crt.AdminFirstName + " " + crt.AdminSurname + " " + crt.AdminSecondSurname
}

func (crt *ContractRenewalTemplateInfo) OwnerFullName() string {
	return crt.OwnerFirstName + " " + crt.OwnerSurname + " " + crt.OwnerSecondSurname
}

func (crt *ContractRenewalTemplateInfo) LesseeFullName() string {
	return crt.LesseeFirstName + " " + crt.LesseeSurname + " " + crt.LesseeSecondSurname
}

func (crt *ContractRenewalTemplateInfo) PropertyName() string {
	if crt.PropertyApartment == "" {
		return crt.PropertyStreet + " " + crt.PropertyStreetNumber + ", " + crt.Commune()
	}
	return crt.PropertyStreet + " " + crt.PropertyStreetNumber + " " + PropertyType[crt.PropertyType] + " " + crt.PropertyApartment + ", " + crt.Commune()

}
func (crt *ContractRenewalTemplateInfo) Commune() string {
	return GetCommune(crt.PropertyCommune)
}

func (crt *ContractRenewalTemplateInfo) EndDate() string {
	return crt.ContractEndsAt[0:10]
}

func (crt *ContractRenewalTemplateInfo) FileName() string {
	if crt.PropertyApartment == "" {
		return crt.PropertyStreet + "-" + crt.PropertyStreetNumber + ".pdf"
	}
	return strings.ReplaceAll(crt.PropertyStreet+"-"+crt.PropertyStreetNumber+"-"+PropertyType[crt.PropertyType]+crt.PropertyApartment, ".", "") + ".pdf"
}

func (crt *ContractRenewalTemplateInfo) LogoUrl() string {
	return "https://files.leasity.cl/" + crt.TenantId + "/assets/summary-brand.png"
}

func (crt *ContractRenewalTemplateInfo) FormatDate() string {
	return Months[crt.Date[5:7]] + " de " + crt.Date[0:4]
}

func (crt *ContractRenewalTemplateInfo) AssetsBaseUrl() string {
	return "http://" + baseURLAssets + "/internal/"
}

func (crt *ContractRenewalTemplateInfo) IsRenewal() bool {
	return crt.RenewalEnabled
}

func (crt *ContractRenewalTemplateInfo) IsEnding() bool {
	return !crt.RenewalEnabled
}

func (crt *ContractRenewalTemplateInfo) LeasityLogoUrl() string {
	return "https://cdn.leasity.cl/assets/images/logo/full-solid-small.png"
}
