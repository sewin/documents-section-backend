package model

var Banks = map[string]string{
	"ABN_AMRO":            "ABR AMRO",
	"BANCO_ARGENTINA":     "Banco de Arentina",
	"BANCO_BRASIL":        "Banco de Brasil",
	"BANCO_CHILE":         "Banco de Chile",
	"BANCO_INTERNACIONAL": "Banco Internacional",
	"BBVA":                "BBVA",
	"BCI":                 "BCI",
	"CONOSUR":             "Conosur",
	"CORPBANCA":           "Corpbanca",
	"DESARROLLO":          "Banco Desarrollo",
	"DEUTSCHE_BANK":       "Deutsche Bank",
	"DRESDNER_BANK":       "Dresdner Bank",
	"FALABELLA":           "Falabella",
	"HSBC":                "HSBC",
	"ITAU":                "Itau",
	"MONEX":               "Consorcio",
	"RIPLEY":              "Riplay",
	"SANTANDER":           "Santander",
	"SCOTIABANK":          "Scotiabank",
	"SECURITY":            "Security",
	"BICE":                "BICE",
	"BANCO_ESTADO":        "Banco Estado",
}

var BankAccountTypes = map[string]string{
	"CC": "Cuenta Corriente",
	"CV": "Cuenta Vista",
	"CA": "Cuenta de Ahorro",
}
