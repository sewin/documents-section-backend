package model

import (
	"database/sql"
	"errors"
	"time"
)

var baseURLAssets string

var ErrNoRowFound = errors.New("no rows match the query")
var ErrNoAssetsBaseUrl = errors.New("can't serve assets with empty base url")

type Entity struct {
	ID        string       `json:"id"`
	CreatedAt time.Time    `json:"created_at"`
	UpdatedAt time.Time    `json:"updated_at"`
	DeletedAt sql.NullTime `json:"deleted_at"`
}

type ReportTemplate interface {
	FileName() string
	GetTenantId() string
}

func SetBaseUrlAssets(url string) error {
	if url == "" {
		return ErrNoAssetsBaseUrl
	}
	baseURLAssets = url
	return nil
}
