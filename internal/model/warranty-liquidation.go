package model

import (
	"strings"
	"time"

	"github.com/leekchan/accounting"
	"github.com/tidwall/gjson"
)

type WarrantyLiquidationTemplateInfo struct {
	PropertyStreet        string
	PropertyStreetNumber  string
	PropertyApartment     string
	PropertyCommune       string
	PropertyType          string
	OwnerFirstName        string
	OwnerSurname          string
	OwnerSecondSurname    string
	OwnerTaxId            string
	BankAccountHolderName string
	BankAccountTaxId      string
	AccountBank           string
	BankAccountNumber     string
	BankAccountType       string
	AccountEmail          string
	AdminFirstName        string
	AdminSurname          string
	AdminSecondSurname    string
	AdminTaxId            string
	Date                  string
	TenantId              string
	CompanyName           string
	CompanyTaxEmail       string
	LesseeFirstName       string
	LesseeSurname         string
	LesseeSecondSurname   string
	ContractEndsAt        string
	OwnerFullName         string
	LesseeFullName        string
	WarrantyEntries       []map[string]interface{}
	PropertyAddress       string
	ContractEndDate       string
	WarrantyAmount        string
	DiscountAmount        string
	PaymentAmount         string
	ContractId            string
	NoWarrantyEntries     bool
}

func (wl *WarrantyLiquidationTemplateInfo) GetTenantId() string {
	return wl.TenantId
}

func WarrantyLiquidationTemplateFromReqJson(entry string) (*WarrantyLiquidationTemplateInfo, error) {
	wlt := WarrantyLiquidationTemplateInfo{}
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}

	wlt.OwnerFullName = gjson.Get(entry, "ownerFullName").String()
	wlt.PropertyAddress = gjson.Get(entry, "propertyAddress").String()
	wlt.LesseeFullName = gjson.Get(entry, "lesseeFullName").String()
	wlt.CompanyName = gjson.Get(entry, "companyName").String()
	wlt.CompanyTaxEmail = gjson.Get(entry, "companyTaxEmail").String()
	wlt.ContractEndDate = gjson.Get(entry, "contractEndDate").String()
	WarrantyAmount := gjson.Get(entry, "warrantyAmount").Int()
	wlt.WarrantyAmount = ac.FormatMoney(WarrantyAmount)

	warrantyentries := gjson.Get(entry, "warrantyEntries")
	wlt.NoWarrantyEntries = len(warrantyentries.Array()) == 0

	entriesmap := make([]map[string]interface{}, 0)
	var DiscountAmount int64 = 0
	for i := 0; i < len(warrantyentries.Array()); i++ {
		entrymap := make(map[string]interface{}, 0)
		entryamount := gjson.Get(warrantyentries.Array()[i].Raw, "amount").Int()
		entrymap["amount"] = ac.FormatMoney(entryamount)
		entrymap["description"] = gjson.Get(warrantyentries.Array()[i].Raw, "description").String()
		entrymap["date"] = gjson.Get(warrantyentries.Array()[i].Raw, "date").String()

		DiscountAmount += entryamount
		entriesmap = append(entriesmap, entrymap)

	}
	wlt.PaymentAmount = ac.FormatMoney(WarrantyAmount - DiscountAmount)
	wlt.DiscountAmount = ac.FormatMoney(DiscountAmount)
	wlt.WarrantyEntries = entriesmap
	wlt.Date = (time.Now()).Format("02-01-2006")
	return &wlt, nil
}

/* func (wlt *WarrantyLiquidationTemplateInfo) PaymentAmount() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}

	return ac.FormatMoney(wlt.WarrantyAmount - wlt.DiscountAmount)
}

func (wlt *WarrantyLiquidationTemplateInfo) FormDiscountAmount() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	return ac.FormatMoney(wlt.WarrantyAmount)
}

func (wlt *WarrantyLiquidationTemplateInfo) FormWarrantyAmount() string {
	ac := accounting.Accounting{Symbol: "$", Precision: 0, Thousand: ".", Decimal: "."}
	return ac.FormatMoney(wlt.DiscountAmount)
} */

func (wlt *WarrantyLiquidationTemplateInfo) AdminFullName() string {

	return wlt.AdminFirstName + " " + wlt.AdminSurname + " " + wlt.AdminSecondSurname
}

func (wlt *WarrantyLiquidationTemplateInfo) Commune() string {
	return GetCommune(wlt.PropertyCommune)
}

func (wlt *WarrantyLiquidationTemplateInfo) EndDate() string {
	return wlt.ContractEndsAt[0:10]
}

func (wlt *WarrantyLiquidationTemplateInfo) FileName() string {
	return strings.ReplaceAll(wlt.PropertyAddress, ".", "") + ".pdf"
}

func (wlt *WarrantyLiquidationTemplateInfo) LogoUrl() string {
	return "https://files.leasity.cl/" + wlt.TenantId + "/assets/summary-brand.png"
}

func (wlt *WarrantyLiquidationTemplateInfo) FormatDate() string {
	return Months[wlt.Date[4:5]] + " de " + wlt.Date[6:10]
}

func (wlt *WarrantyLiquidationTemplateInfo) AssetsBaseUrl() string {
	return "http://" + baseURLAssets + "/internal/"
}

func (wlt *WarrantyLiquidationTemplateInfo) LeasityLogoUrl() string {
	return "https://cdn.leasity.cl/assets/images/logo/full-solid-small.png"
}
