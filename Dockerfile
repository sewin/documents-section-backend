FROM golang:1.17 as builder

WORKDIR /go/src
COPY ./ ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/start .

FROM alpine

WORKDIR /app
COPY --from=builder /go/bin .
COPY ./templates ./templates
CMD ["./start"]