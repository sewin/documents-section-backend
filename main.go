package main

import (
	"os"

	"leasity.cl/backends/documents-section/server"
)

func main() {

	os.Setenv("PDF_ENGINE_URL", "172.17.0.3:8080")
	os.Setenv("DB_HOST", "172.17.0.2")
	os.Setenv("DB_PORT", "5432")
	os.Setenv("DB_USER", "leasity")
	os.Setenv("DB_USER_PASSWORD", "leasity123")
	os.Setenv("DB_NAME", "leasity")
	os.Setenv("PAYMENTS_DB_NAME", "payments")
	os.Setenv("ASSETS_BASE_URL", "172.17.0.1:8080")

	fserver := server.NewFastHttpServer("0.0.0.0:8080")
	fserver.ListenAddr = "0.0.0.0:8080"
	fserver.Run()

}
